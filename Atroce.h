#include "main.h"

class Atroce
{
public:
	Atroce();

	SDL_Rect monsterPos, monsterAnim;

	int frame_x;
	int frame_y;
	int death_frame_x;
	double xVel;
	int yVel;
	int points;
	int hp;    //health points

	bool monsterDead, monsterDying;

	double posX, posY;
	double posDelta;
	double deathDelta;

	void walk(double delta);
	bool killedMonster(double delta);

private:



};