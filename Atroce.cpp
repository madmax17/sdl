#include "Atroce.h"

Atroce::Atroce()
{
	posX = 800;
	posY = 560;

	xVel = 20;
	yVel = 30;

	frame_x = 0;
	frame_y = 0;
	posDelta = 0;
	deathDelta = 0;

	death_frame_x = 0;

	points = 10;
	hp = 30;

	monsterPos.x = posX;
	monsterPos.y = posY;
	monsterPos.h = 155;
	monsterPos.w = 134;

	monsterAnim.x = posX;
	monsterAnim.y = posY;
	monsterAnim.h = 155;
	monsterAnim.w = 134;

	monsterDead = false;
	monsterDying = false;
}

void Atroce::walk(double delta)
{
	frame_x = 0;
	frame_y = 0;

	posX-=xVel*delta;//movement

	posDelta+=delta;//animation
	frame_x = (int)posDelta;

	if(frame_x > 7)
	{
		frame_y = 0;
		frame_x = 0;
		posDelta = 0;
	}
}

bool Atroce::killedMonster(double delta)
{
	frame_y = 1;
	death_frame_x = 0;

	deathDelta+=(int)delta; //animation
	death_frame_x = (int)deathDelta;

	if(death_frame_x > 4)
	{
		return true;
	}
	return false;

}