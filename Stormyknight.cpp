#include "Stormyknight.h"

Stormyknight::Stormyknight()
{
	posX = 800;
	posY = 560;

	xVel = 20;
	yVel = 30;

	frame_x = 0;
	frame_y = 0;
	posDelta = 0;
	deathDelta = 0;

	death_frame_x = 0;

	points = 15;
	hp = 50;

	monsterPos.x = posX;
	monsterPos.y = posY;
	monsterPos.h = 158;
	monsterPos.w = 180;

	monsterAnim.x = posX;
	monsterAnim.y = posY;
	monsterAnim.h = 158;
	monsterAnim.w = 180;

	monsterDead = false;
	monsterDying = false;
}

void Stormyknight::walk(double delta)
{
	frame_x = 0;
	frame_y = 0;

	posX-=xVel*delta;//movement

	posDelta+=delta;//animation
	frame_x = (int)posDelta;

	if(frame_x > 5)
	{
		frame_y = 0;
		frame_x = 0;
		posDelta = 0;
	}
}

bool Stormyknight::killedMonster(double delta)
{
	frame_y = 1;
	death_frame_x = 0;

	deathDelta+=(int)delta; //animation
	death_frame_x = (int)deathDelta;

	if(death_frame_x > 4)
	{
		return true;
	}
	return false;

}