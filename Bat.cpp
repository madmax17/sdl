#include "Bat.h"

Bat::Bat()
{

	posX = 0;
	posY = 400;

	xVel = 1;
	yVel = 30;

	frame_x = 0;
	frame_y = 0;
	posDelta = 0;

	monsterPos.x = posX;
	monsterPos.y = posY;
	monsterPos.h = 56;
	monsterPos.w = 64;

	monsterAnim.x = posX;
	monsterAnim.y = posY;
	monsterAnim.h = 56;
	monsterAnim.w = 64;

	movingLeft = true;
	movingRight = false;
}

void Bat::moveLeft(double delta)
{
	frame_x = 0;
	frame_y = 0;

	posX-=xVel*delta;//movement

	posDelta+=delta;//animation
	frame_x = (int)posDelta;

	if(frame_x > 7)
	{
		frame_y = 0;
		frame_x = 0;
		posDelta = 0;
	}
}

void Bat::moveRight(double delta)
{
	frame_x = 0;
	frame_y = 1;

	posX+=xVel*delta;//movement

	posDelta+=delta; //animation
	frame_x = (int)posDelta;

	if(frame_x > 7)
	{
		frame_y = 1;
		frame_x = 0;
		posDelta = 0;
	}

}