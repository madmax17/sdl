#include "Game.h"

Game::Game()
{
	shooting = false;
	gameover = true;
	inAir = false;
	sound = true;
	box_clicked = false;
	instructions_clicked = false;
	hit = false;
	entered_timer = false;
	player_dead = false;
	lock_points_playerdead = false;

	bottom = (SCREEN_HEIGHT/3/4);
	FRAMES_PER_SECOND = 120;

	acceleration = 5;
	gravity = 4;
	points = 0;
	level = 1;
	bullet_strength = 5;

	/* set player position */
	rcSprite.x = 0;
	rcSprite.y = SCREEN_HEIGHT-(SCREEN_HEIGHT/3/4)-123;

	frame = 0;

	shot_effect = NULL;

	rcInstructions.x=0;
	rcInstructions.y=0;

	rcCheck.x=SCREEN_WIDTH-350;
	rcCheck.y=0;
	rcCheck.w=20;
	rcCheck.h=20;

	rcCheck_label.x=rcCheck.x+24;

	rcCheck_instructions.x=SCREEN_WIDTH-580;

	/*Game over text position and size*/
	rcCheck_gameover.x = SCREEN_WIDTH/2/3+140;
	rcCheck_gameover.y = SCREEN_HEIGHT/2/3;

	/*Display total*/
	rcCheck_total.x = SCREEN_WIDTH/2/3+175;
	rcCheck_total.y = SCREEN_HEIGHT/2/3+100;

	/*Display points*/
	rcCheck_points.x = SCREEN_WIDTH/2/3+485;
	rcCheck_points.y = SCREEN_HEIGHT/2/3+100;

	/*bat position*/
	bat.posX = SCREEN_WIDTH-64;
	bat.posY = 20;
}


Game::~Game()
{
	
}

int showmenu(SDL_Surface* screen, TTF_Font* font)
{
	Uint32 time;
	int x,y;
	const int NUMMENU = 2;
	const char* labels[NUMMENU] = {"Play","Quit"};
	SDL_Surface* menus[NUMMENU];
	bool selected[NUMMENU] = {0,0};
	SDL_Color color[2] = {{255,255,255},{255,0,0}};

	menus[0] = TTF_RenderText_Solid(font,labels[0],color[0]);
	menus[1] = TTF_RenderText_Solid(font,labels[1],color[0]);

	SDL_Rect pos[NUMMENU];

	/*menu position*/
	pos[0].x = screen->clip_rect.w/2 - menus[0]->clip_rect.w/2;
	pos[0].y = screen->clip_rect.h/2.4 - menus[0]->clip_rect.h;
	pos[1].x = screen->clip_rect.w/2 - menus[0]->clip_rect.w/2;
	pos[1].y = screen->clip_rect.h/2.2 + menus[0]->clip_rect.h;

	/*make a black screen*/
	SDL_FillRect(screen,&screen->clip_rect,SDL_MapRGB(screen->format,0x00,0x00,0x00));

	SDL_Event event;
	while(1)
	{
		time = SDL_GetTicks();
		while(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
			case SDL_QUIT:
				SDL_FreeSurface(menus[0]);
				SDL_FreeSurface(menus[1]);
				break;
			case SDL_MOUSEMOTION:
				x = event.motion.x;
				y = event.motion.y;
				for(int i = 0; i < NUMMENU; i += 1) {
					if(x>=pos[i].x && x<=pos[i].x+pos[i].w && y>=pos[i].y && y<=pos[i].y+pos[i].h)
					{
						if(!selected[i])
						{
							selected[i] = 1;
							SDL_FreeSurface(menus[i]);
							menus[i] = TTF_RenderText_Solid(font,labels[i],color[1]);
						}
					}
					else
					{
						if(selected[i])
						{
							selected[i] = 0;
							SDL_FreeSurface(menus[i]);
							menus[i] = TTF_RenderText_Solid(font,labels[i],color[0]);
						}
					}
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				x = event.button.x;
				y = event.button.y;
				for(int i = 0; i < NUMMENU; i += 1) {
					if(x>=pos[i].x && x<=pos[i].x+pos[i].w && y>=pos[i].y && y<=pos[i].y+pos[i].h)
					{
						SDL_FreeSurface(menus[0]);
						SDL_FreeSurface(menus[1]);
						return i;
					}
				}
				break;
			case SDL_KEYDOWN:
				if(event.key.keysym.sym == SDLK_ESCAPE)
				{
					SDL_FreeSurface(menus[0]);
					SDL_FreeSurface(menus[1]);
					return 1;
				}
			}
		}
		for(int i = 0; i < NUMMENU; i += 1) {
			SDL_BlitSurface(menus[i],NULL,screen,&pos[i]);
		}
		SDL_Flip(screen);
	}
}

void Game::Initialize()
{
	 //create window 
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SPRITE_SIZE, SDL_SWSURFACE | SDL_DOUBLEBUF);

	// initialize SDL 
	SDL_Init(SDL_INIT_VIDEO);

	/*load player*/
	temp   = SDL_LoadBMP("res/sprite.bmp");
	sprite = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* setup sprite colorkey and turn on RLE */
	colorkey = SDL_MapRGB(screen->format, 255, 0, 255);
	SDL_SetColorKey(sprite, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);			 //remove background from image

	/* load background */
	temp = SDL_LoadBMP("res/background.bmp");
	background = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* load bullet */
	temp = SDL_LoadBMP("res/bullet.bmp");
	bullet = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/*load brick*/
	temp = SDL_LoadBMP("res/brick.bmp");
	brick = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* setup sprite colorkey and turn on RLE */
	colorkey = SDL_MapRGB(screen->format, 255, 255, 255);
	SDL_SetColorKey(bullet, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);			 //remove background from image

	/*load instructions*/
	temp = SDL_LoadBMP("res/instructions.bmp");
	instructions = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	//set the title bar 
	SDL_WM_SetCaption("SDL Game", NULL);

	/*init sounds*/
	Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096);

	Mix_Init(MIX_INIT_OGG);

	music = Mix_LoadMUS("res/music.ogg");
	shot_effect = Mix_LoadWAV("res/shot.wav");
	anolian_shout = Mix_LoadWAV("res/anolian.wav");
	hit_sound = Mix_LoadWAV("res/hit.wav");
	atroce_shout = Mix_LoadWAV("res/atroce.wav");

	/*lower sound*/
	Mix_VolumeChunk(anolian_shout, 10);

	/*lower sound*/
	Mix_VolumeChunk(atroce_shout, 10);

	TTF_Init();
	font = TTF_OpenFont("res/kristen.ttf",30);

	font_check = TTF_OpenFont("res/kristen.ttf",20);

	font_gameover = TTF_OpenFont("res/gothic.ttf",80);

	/*load anolian*/
	temp   = SDL_LoadBMP("res/wraith.bmp");
	anolian_surface = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* setup sprite colorkey and turn on RLE */
	colorkey = SDL_MapRGB(screen->format, 0, 128, 128);
	SDL_SetColorKey(anolian_surface, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);			 //remove background from image

	/*load bat*/
	temp = SDL_LoadBMP("res/bat.bmp");
	bat_surface = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* setup sprite colorkey and turn on RLE */
	colorkey = SDL_MapRGB(screen->format, 255, 255, 255);
	SDL_SetColorKey(bat_surface, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);			 //remove background from image

	/*load atroce*/
	temp = SDL_LoadBMP("res/atroce.bmp");
	atroce_surface = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* setup sprite colorkey and turn on RLE */
	colorkey = SDL_MapRGB(screen->format, 64, 128, 128);
	SDL_SetColorKey(atroce_surface, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);			 //remove background from image

	/*load stormy knight*/
	temp = SDL_LoadBMP("res/stormyknight.bmp");
	stormyknight_surface = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* setup sprite colorkey and turn on RLE */
	colorkey = SDL_MapRGB(screen->format, 64, 128, 128);
	SDL_SetColorKey(stormyknight_surface, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);			 //remove background from image
}

void Game::Gameloop()
{
	m = showmenu(screen, font);

	if(m==0)
		gameover = false;

	startClocks();

	while (!gameover)
	{
		/* look for an event */
		if (SDL_PollEvent(&event)) {
			/* an event was found */
			switch (event.type) {
				/* close button clicked */
			case SDL_QUIT:
				gameover = true;
				break;

			case SDL_KEYDOWN:
				switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
					gameover = true;
					break;
				}
				break;

			case SDL_MOUSEBUTTONDOWN:
				x = event.motion.x;
				y = event.motion.y;
				/*if mouse pressed on mute box*/
				if(x>=rcCheck.x && x<=rcCheck.x+rcCheck.w && y>=rcCheck.y && y<=rcCheck.y+rcCheck.h)
				{
					sound = !sound;
					box_clicked = !box_clicked;
				}
				/*if mouse pressed on instructions*/
				if(x>=rcCheck_instructions.x && x<=rcCheck_instructions.x+rcCheck_instructions.w && y>=rcCheck_instructions.y 
					&& y<=rcCheck_instructions.y+rcCheck_instructions.h)
				{
					instructions_clicked = !instructions_clicked;
				}

				break;
			}
		}
		
		/*play music*/
		if(sound)
		{   
			/*if music not playing*/
			if( Mix_PlayingMusic() == 0 )
				Mix_PlayMusic(music, -1);

			//If the music is paused
			if( Mix_PausedMusic() == 1 )
			{
				//Resume the music
				Mix_ResumeMusic();
			}
		}

		/*pause music if sound false*/
		if(!sound)
			Mix_PauseMusic();

	/*if player dead disable controls*/
	if(!player_dead)
	{
		/* handle sprite movement */
		keystate = SDL_GetKeyState(NULL);

		if (keystate[SDLK_LEFT] ) {
			rcSprite.x -= 2;
		}

		if (keystate[SDLK_RIGHT] ) {
			rcSprite.x += 2;
		}
		
		if (keystate[SDLK_DOWN] ) {
			rcSprite.y += 2;
		}

		if (keystate[SDLK_SPACE]){
			shooting = true;
		}

		/*falling*/
		if(rcSprite.y>bottom)
		{
			rcSprite.y += 1;
			rcSprite.x += 0.5;
		}

		/*jumping*/
		if (keystate[SDLK_UP]&&(!inAir)&&(jump.get_ticks()<200.f)) 
			{
				if(!jump.is_started())
					jump.start();
			    rcSprite.y -= gravity;
			}

		/*stop jumping*/
		if((jump.get_ticks()>500.f))
		{
			inAir = true;
			if(jump.is_started())
				jump.stop();
		}

		/*when player hits bottom*/
		if((rcSprite.y+32==rcPlatform.y) || (rcSprite.y+32>=rcPlatform.y))
			inAir = false;

   /*	if(collision(&rcSprite,&rcPlatform))
			inAir = false;*/
	}

		/* collide with edges of screen */
		if ( rcSprite.x < 0 ) {
			rcSprite.x = 0;
		}
		else if ( rcSprite.x > SCREEN_WIDTH-SPRITE_SIZE ) {
			rcSprite.x = SCREEN_WIDTH-SPRITE_SIZE;
		}
		if ( rcSprite.y < 0 ) {
			rcSprite.y = 0;
		}
		else if ( rcSprite.y > SCREEN_HEIGHT-SPRITE_SIZE-bottom ) {
			rcSprite.y = SCREEN_HEIGHT-SPRITE_SIZE-bottom;
		}

		if (rcSprite.y > SCREEN_HEIGHT-SPRITE_SIZE-bottom){


		}

		/*update bullet position in front of player but only if not shooting*/
		if (!shooting)
		{
			rcBullet.x = rcSprite.x+1;
			rcBullet.y = rcSprite.y+13;

			rcMuzzle.x = rcSprite.x;
			rcMuzzle.y = rcSprite.y;
		}

		/* draw the bullet */
		if (shooting && rcBullet.x<SCREEN_WIDTH)
		{
			rcBullet.x += acceleration;
			/*shooting sound*/
			if(sound)
				Mix_PlayChannel(-1, shot_effect, 0);
		}
		else
			shooting = false;

		switch(level)
		{
		  case	1:
			  not_monster_bat();
			  monster();
			  break;

		  case 2:
			  not_monster_bat();
			  monster_atroce();
			  break;
		  case 3:
			  not_monster_bat();
			  monster_stormyknight();
			  break;
		}
		
		if( update.get_ticks() > 1000 )
		{
			std::stringstream caption;

			caption<<"Average FPS: " << frame / ( fps.get_ticks() / 1000.f )<<std::setw(110)<<"Level: "<<level<<std::setw(60)<<"Points: "<<points;

			//Reset the caption
			SDL_WM_SetCaption( caption.str().c_str(), NULL );

			//Restart the update timer
			update.start();
		}	

		draw();

		//Cap the frame rate
		/*if( frame/fps.get_ticks()/1000 < 1000 / FRAMES_PER_SECOND )
		{
			SDL_Delay( ( 1000 / FRAMES_PER_SECOND ));
		}*/

		/*int delay=1000/FRAMES_PER_SECOND-SDL_GetTicks()+global.get_ticks();

		if(delay>0)
			SDL_Delay(delay);*/

/*		if (1000/FRAMES_PER_SECOND>SDL_GetTicks()-global.get_ticks())
			SDL_Delay(1000/FRAMES_PER_SECOND-(SDL_GetTicks()-global.get_ticks()));*/
		frame++;

		if(delta.get_ticks() > 10000)
			delta.start();

		/*if monster dead for over 5 seconds*/
		if(monsterDead.get_ticks() > 5000)
		{
			reset();
			/*move to next level*/
			level++;
		}

		if(level == 4)
			player_dead = true;

		if(player_dead)
			freezeClocks();

		/*don't update points or let the player die if monster is dead*/
		if(monsterDead.is_started())
			lock_points_playerdead = true;
	}

	destroy();
}

void Game::draw()
{
	/* draw background */
	SDL_BlitSurface(background, NULL, screen, NULL);

	/* draw bullet */
	if (shooting)
	{
		SDL_BlitSurface(bullet, NULL, screen, &rcBullet);
	}

	/* draw player*/
	SDL_BlitSurface(sprite, NULL, screen, &rcSprite);

	/* draw brick*/
	rcPlatform.y = SCREEN_HEIGHT-(SCREEN_HEIGHT/3/4);

	for (int x = 0; x < SCREEN_WIDTH / SPRITE_SIZE; x++) {
		rcPlatform.x = x * SPRITE_SIZE;
		SDL_BlitSurface(brick, NULL, screen, &rcPlatform);
	}

	/*draw box*/
	if(!box_clicked)
		FillRect(rcCheck.x,rcCheck.y,rcCheck.w,rcCheck.h,0xFFFFFF);
	else
		FillRect(rcCheck.x,rcCheck.y,rcCheck.w,rcCheck.h,0x000000);
	/*draw instructions*/
	if(instructions_clicked)
		SDL_BlitSurface(instructions, NULL, screen, &rcInstructions);

	SDL_Color color_white = {255,255,255};

	text_surface = TTF_RenderText_Solid(font_check,"Mute",color_white);
	/*draw label*/
	SDL_BlitSurface(text_surface, NULL, screen, &rcCheck_label);

	text_instructions = TTF_RenderText_Solid(font_check, "Instructions", color_white);
	/*draw label*/
	SDL_BlitSurface(text_instructions, NULL, screen, &rcCheck_instructions);

	/*if more than 2 seconds after monster dead stop drawing*/
	if(monsterDead.get_ticks() < 2000)
	{
		switch (level)
		{
			/*draw anolian*/
			case 1: 
				SDL_BlitSurface(anolian_surface, &anolian.monsterAnim, screen, &anolian.monsterPos);
				break;
			/*draw atroce*/
			case 2:
				SDL_BlitSurface(atroce_surface, &atroce.monsterAnim, screen, &atroce.monsterPos);
				break;
			/*draw stormyknight*/
			case 3:
				SDL_BlitSurface(stormyknight_surface, &stormyknight.monsterAnim, screen, &stormyknight.monsterPos);
				break;
		}
	}

	/*draw bat*/
	SDL_BlitSurface(bat_surface, &bat.monsterAnim, screen, &bat.monsterPos);

	if(player_dead)
	{
		text_gameover = TTF_RenderText_Solid(font_gameover, "Game Over", color_white);
		/*draw text*/
		SDL_BlitSurface(text_gameover, NULL, screen, &rcCheck_gameover);

		text_total =  TTF_RenderText_Solid(font_check, "Your total number of points is: ", color_white);
		/*draw text*/
		SDL_BlitSurface(text_total, NULL, screen, &rcCheck_total);

		/*convert points into char*/
		std::stringstream strs;
		strs << points;
		std::string temp_str = strs.str();
		char* char_type = (char*) temp_str.c_str();

		text_points =  TTF_RenderText_Solid(font_check, char_type, color_white);
		/*draw text*/
		SDL_BlitSurface(text_points, NULL, screen, &rcCheck_points);
	}

	/* update the screen */
	SDL_UpdateRect(screen, 0, 0, 0, 0);
	//SDL_Flip(screen);
}

void Game::destroy()
{
	SDL_FreeSurface(sprite);
	SDL_FreeSurface(bullet);
	SDL_FreeSurface(background);
	SDL_FreeSurface(brick);
	SDL_FreeSurface(bat_surface);
	SDL_FreeSurface(instructions);
	SDL_FreeSurface(text_surface);
	SDL_FreeSurface(text_instructions);
	SDL_FreeSurface(anolian_surface);
	SDL_FreeSurface(screen);
	SDL_FreeSurface(atroce_surface);
	SDL_FreeSurface(stormyknight_surface);

	Mix_FreeMusic(music);
	Mix_FreeChunk(shot_effect);
	Mix_FreeChunk(anolian_shout);
	Mix_FreeChunk(hit_sound);
	Mix_FreeChunk(atroce_shout);
	Mix_CloseAudio();

	TTF_CloseFont(font);
	TTF_CloseFont(font_check);
	TTF_CloseFont(font_gameover);
	TTF_Quit();

	SDL_Quit();
}

void Game::startClocks()
{
	delta.start();
	update.start();
	fps.start();
	global.start();
}

void Game::freezeClocks()
{
	delta.stop();
	update.stop();
	fps.stop();
	global.stop();
}

void Game::FillRect(int x, int y, int w, int h, int color) {
	SDL_Rect rect = {x,y,w,h};
	SDL_FillRect(screen, &rect, color);
}

void Game::monster()
{
	/*one second passed in game*/
	if(global.get_ticks() > 1000)
	{
		if(global.get_ticks() < 2000)
		{
		if(sound)
			Mix_PlayChannel(-1, anolian_shout, 0);
		}

		if((anolian.posX > rcSprite.x)&&!anolian.monsterDying)
			anolian.walk(update.get_ticks()/1000);

		anolian.monsterPos.x=anolian.posX;
		anolian.monsterPos.y=anolian.posY;
		anolian.monsterPos.h=150;
		anolian.monsterPos.w=100;

		anolian.monsterAnim.x=anolian.frame_x*100;
		anolian.monsterAnim.y=anolian.frame_y*150;
		anolian.monsterAnim.h=150;
		anolian.monsterAnim.w=100;

		/*if bullet hits target*/
		if(collision(&rcBullet, &anolian.monsterPos)&&shooting)
			hit = true;

		/*if player is hit*/
		if(collision(&rcSprite, &anolian.monsterPos) && !lock_points_playerdead)
			player_dead = true;

		if(hit && !lock_points_playerdead)
		{
			/*play bullet hit sound*/
			if(sound)
				Mix_PlayChannel(2, hit_sound, 0);

			points+=anolian.points;
			anolian.hp-=bullet_strength;

			hit=false;
			shooting=false;
		}

		/*go into death animation*/
		if(anolian.hp<=0)
		{
			if(anolian.killedMonster(update.get_ticks()/1000))
				anolian.monsterDying = true;
		}

		if(anolian.monsterDying && !entered_timer)
		{
			monsterDead.start();
			entered_timer = true;
		}
	}
}

void Game::monster_atroce()
{
	/*one second passed in game*/
	if(global.get_ticks() > 1000)
	{
		if(global.get_ticks() < 2000)
		{
			if(sound)
				Mix_PlayChannel(-1, atroce_shout, 0);
		}

		if((atroce.posX > rcSprite.x)&&!atroce.monsterDying)
			atroce.walk(update.get_ticks()/1000);

		atroce.monsterPos.x=atroce.posX;
		atroce.monsterPos.y=atroce.posY;
		atroce.monsterPos.h=155;
		atroce.monsterPos.w=134;

		atroce.monsterAnim.x=atroce.frame_x*134;
		atroce.monsterAnim.y=atroce.frame_y*155;
		atroce.monsterAnim.h=155;
		atroce.monsterAnim.w=134;

		/*if bullet hits target*/
		if(collision(&rcBullet, &atroce.monsterPos)&&shooting)
			hit = true;

		/*if player is hit*/
		if(collision(&rcSprite, &atroce.monsterPos) && !lock_points_playerdead)
			player_dead = true;

		if(hit && !lock_points_playerdead)
		{
			/*play bullet hit sound*/
			if(sound)
				Mix_PlayChannel(2, hit_sound, 0);

			points+=atroce.points;
			atroce.hp-=bullet_strength;

			hit=false;
			shooting=false;
		}

		/*go into death animation*/
		if(atroce.hp<=0)
		{
			if(atroce.killedMonster(update.get_ticks()/1000))
				atroce.monsterDying = true;
		}

		if(atroce.monsterDying && !entered_timer)
		{
			monsterDead.start();
			entered_timer = true;
		}
	}
}

void Game::monster_stormyknight()
{
	/*one second passed in game*/
	if(global.get_ticks() > 1000)
	{
		if(global.get_ticks() < 2000)
		{
			if(sound)
				Mix_PlayChannel(-1, atroce_shout, 0);
		}

		if((stormyknight.posX > rcSprite.x)&&!stormyknight.monsterDying)
			stormyknight.walk(update.get_ticks()/1000);

		stormyknight.monsterPos.x=stormyknight.posX;
		stormyknight.monsterPos.y=stormyknight.posY;
		stormyknight.monsterPos.h=158;
		stormyknight.monsterPos.w=180;

		stormyknight.monsterAnim.x=stormyknight.frame_x*180;
		stormyknight.monsterAnim.y=stormyknight.frame_y*158;
		stormyknight.monsterAnim.h=158;
		stormyknight.monsterAnim.w=180;

		/*if bullet hits target*/
		if(collision(&rcBullet, &stormyknight.monsterPos)&&shooting)
			hit = true;

		/*if player is hit*/
		if(collision(&rcSprite, &stormyknight.monsterPos) && !lock_points_playerdead)
			player_dead = true;

		if(hit && !lock_points_playerdead)
		{
			/*play bullet hit sound*/
			if(sound)
				Mix_PlayChannel(2, hit_sound, 0);

			points+=stormyknight.points;
			stormyknight.hp-=bullet_strength;

			hit=false;
			shooting=false;
		}

		/*go into death animation*/
		if(stormyknight.hp<=0)
		{
			if(stormyknight.killedMonster(update.get_ticks()/1000))
				stormyknight.monsterDying = true;
		}

		if(stormyknight.monsterDying && !entered_timer)
		{
			monsterDead.start();
			entered_timer = true;
		}
	}
}

bool Game::collision(SDL_Rect *a, SDL_Rect *b)
{
	if( a->x < b->x + b->w &&
		a->x + a->w > b->x &&
		a->y < b->y + b->h &&
		a->y + a->h > b->y )
	{
		return true;
	}
	else
		return false;
}

void Game::reset()
{
	monsterDead.stop();
	startClocks();
	entered_timer = false;
	lock_points_playerdead = false;
}

void Game::not_monster_bat()
{
	if(bat.movingLeft)
	{
		bat.moveLeft(delta.get_ticks()/1000);

		bat.monsterPos.x=bat.posX;
		bat.monsterPos.y=bat.posY;
		bat.monsterPos.h=56;
		bat.monsterPos.w=64;

		bat.monsterAnim.x=bat.frame_x*64;
		bat.monsterAnim.y=bat.frame_y*56;
		bat.monsterAnim.h=56;
		bat.monsterAnim.w=64;
	}

	if(bat.posX <= 1)
	{
		bat.movingLeft = false;
		bat.movingRight = true;
	}


	if(bat.posX >= SCREEN_WIDTH)
	{
		bat.movingLeft = true;
		bat.movingRight = false;
	}

	if (bat.movingRight)
	{
		bat.moveRight(delta.get_ticks()/1000);

		bat.monsterPos.x=bat.posX;
		bat.monsterPos.y=bat.posY;
		bat.monsterPos.h=56;
		bat.monsterPos.w=64;

		bat.monsterAnim.x=bat.frame_x*64;
		bat.monsterAnim.y=bat.frame_y*56;
		bat.monsterAnim.h=56;
		bat.monsterAnim.w=64;
	}
}