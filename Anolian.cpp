#include "Anolian.h"

Anolian::Anolian()
{
	posX = 800;
	posY = 580;

	xVel = 15;
	yVel = 30;

	frame_x = 0;
	frame_y = 0;
	posDelta = 0;
	deathDelta = 0;

	death_frame_x = 0;

	points = 5;
	hp = 20;

	monsterPos.x = posX;
	monsterPos.y = posY;
	monsterPos.h = 150;
	monsterPos.w = 100;

	monsterAnim.x = posX;
	monsterAnim.y = posY;
	monsterAnim.h = 150;
	monsterAnim.w = 100;

	monsterDead = false;
	monsterDying = false;
}

void Anolian::walk(double delta)
{
	frame_x = 0;
	frame_y = 0;

	posX-=xVel*delta;//movement

	posDelta+=delta;//animation
	frame_x = (int)posDelta;

	if(frame_x > 6)
	{
		frame_y = 0;
		frame_x = 0;
		posDelta = 0;
	}
}

bool Anolian::killedMonster(double delta)
{
	frame_y = 2;
	death_frame_x = 0;

	deathDelta+=(int)delta; //animation
	death_frame_x = (int)deathDelta;

	if(death_frame_x > 2)
	{
		return true;
	}
	return false;

}