#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>
#include <string>
#include <iomanip>		 //setw()
#include <sstream>
#include "Anolian.h"
#include "Atroce.h"
#include "Bat.h"
#include "main.h"
#include "Timer.h"
#include "Stormyknight.h"

class Game
{
	public:
		Game();
		~Game();

		void Initialize();
		void Gameloop();

	private:
		Anolian anolian;
		Atroce atroce;
		Bat bat;
		Stormyknight stormyknight;

		Mix_Chunk *shot_effect, *anolian_shout, *hit_sound, *atroce_shout;
		Mix_Music *music;

		SDL_Surface *screen, *sprite, *temp, *bullet, *background, *brick, *bat_surface, *instructions, *text_surface, *text_instructions, 
			*anolian_surface, *atroce_surface, *stormyknight_surface, *text_gameover, *text_points, *text_total;
		SDL_Rect rcSprite, rcBullet, rcPlatform, rcInstructions, rcCheck, rcCheck_label, rcCheck_instructions, rcMuzzle, rcCheck_gameover,
			rcCheck_points, rcCheck_total;

		SDL_Event event;
		Uint8 *keystate;
		int colorkey, bottom, frame, FRAMES_PER_SECOND;
		int acceleration, points, level, bullet_strength, gravity;

		TTF_Font *font, *font_check, *font_gameover;
		int m;
		int x, y;
		SDL_Color color_white;

		Timer delta;
		Timer update;
		Timer fps;
		Timer global;
		Timer monsterDead;
		Timer jump;

		bool shooting, gameover, inAir, sound, box_clicked, instructions_clicked, hit, entered_timer,
			player_dead, lock_points_playerdead;

		void destroy();
		void draw();
		void startClocks();
		void freezeClocks();
		int  showMenu(SDL_Surface* screen, TTF_Font* font);
		void FillRect(int x, int y, int w, int h, int color);
		void monster();
		bool collision(SDL_Rect *a, SDL_Rect *b);
		void reset();
		void not_monster_bat();
		void monster_atroce();
		void monster_stormyknight();
};
